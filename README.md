I've rewritten the Levenberg-Marquardt algorithm in Python, for purposes of fitting experimental data used in my bachelor thesis. 
Due to scarceness of elegant and flexible Python LM algorithms with implemented boundaries, I've decided to implement
this Matlab code from Henri P.Gavin in Python, which can be found in this document : http://people.duke.edu/~hpgavin/ce281/lm.pdf.

I do not claim this code, nor any intellectual property used in writing the original Matlab code. I have simply implemented the already publicly available Matlab code in Python, which is free, opposed to licenced Matlab. I hope this program will help someone, struggling with non-linear fitting.

If the original authors wish me to remove this from the public domain, I will gladly do so. For any other questions, regarding code, I'm open to answering your questions, albeit with possible delay.
