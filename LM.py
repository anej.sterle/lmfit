import torch
import numpy as np
from copy import copy
import matplotlib.pyplot as plt

 # Funkcija za računanje nelinearnih problemov Levenberg - Marquadt - Python implementacija
 # Anej Sterle, 2020


def LM(fun, x, y, p, dp = 0.001, weight = None, pmin = None, pmax = None,
       verbose = True, epsG = 1e-4, epsP = 1e-4, epsChi = 1e-4, epsL = 1e-1,
       MaxIter = 200, lam0 = 1e-2, lamU = 11, lamD = 9, UpdateType = 1,
       args = (), kwargs = {}):

    '''
    Model presumes we have m independent variables and n parameters.
    
    ------------ INPUT -----------:

    fun : fun(p, *args, **kwargs) - function returning the y_model,                 (m x 1) array
    p   : initial guess of parameters                                               (n x 1) array
    dp  : fractional increment of 'p' for numerical derivatives                     float
        - dp(j)>0 central differences calculated
          Default:  0.005
        # Others not yet implemented
    weight :  weights or a scalar weight value ( weight >= 0 ) ...                  (m x 1) array
              inverse of the standard measurement errors
              Default:  ( 1 / ( y_dat' * y_dat ))           # Not sure
              Default: 1
    p_min :  lower bounds for parameter values                                      (n x 1) array
             Default: -np.inf
    
    p_max :  upper bounds for parameter values                                      (n x 1) array
    
    ADDITIONAL PARAMETERS:
    parameter    defaults    meaning
    verbose         True     True: printing messages, False: no messages
    MaxIter         200    maximum number of iterations
    epsG            1e-4     convergence tolerance for gradient
    epsP            1e-4     convergence tolerance for parameters
    epsChi          1e-4     convergence tolerance for red. Chi-square
    epsL            1e-1     determines acceptance of a L-M step
    lam0            1e-2     initial value of L-M paramter
    lamU            11       factor for increasing lambda
    lamD            9       factor for decreasing lambda
    UpdateType      1       1: Levenberg-Marquardt lambda update
                             2: Quadratic update
                             3: Nielsen's lambda update equations

    ------------ OUTPUT -----------:
    p       = least-squares optimal estimate of the parameter values
    redX2   = reduced Chi squared error criteria - should be close to 1
    sigma_p = asymptotic standard error of the parameters
    sigma_y = asymptotic standard error of the curve-fit
    corr_p  = correlation matrix of the parameters
    R_sq    = R-squared cofficient of multiple determination
    cvg_hst = convergence history ... see lm_plots.m
    '''
    # ------------------ FUNCTIONS -----------------------

    def lm_matx(func, t, p_old, y_old, dX2, J, p, y_dat, weight, dp, args, kwargs):
        '''
         Evaluate the linearized fitting matrix, JtWJ, and vector JtWdy,
         and calculate the Chi-squared error function, Chi_sq
         Used by Levenberg-Marquard algorithm, lm.m
         -------- INPUT VARIABLES ---------
         func   = function ofpn independent variables, p, and m parameters, p,
                 returning the simulated model: y_hat = func(t,p,c)
         t      = independent variables (used as arg to func)                   (m x 1)
         p_old  = previous parameter values                                     (n x 1)
         y_old  = previous model ... y_old = y_hat(t;p_old);                    (m x 1)
         dX2    = previous change in Chi-squared criteria                       (1 x 1)
         J      = Jacobian of model, y_hat, with respect to parameters, p       (m x n)
         p      = current  parameter values                                     (n x 1)
         y_dat  = data to be fit by func(t,p,c)                                 (m x 1)
         weight = the weighting vector for least squares fit ...
                  inverse of the squared standard measurement errors
         dp     = fractional increment of 'p' for numerical derivatives
                  dp(j)>0 central differences calculated
                  dp(j)<0 one sided differences calculated
                  dp(j)=0 sets corresponding partials to zero; i.e. holds p(j) fixed
                  Default:  0.001;
         args     = optional args for func f(t,p,*args, **kwargs)
         kwargs   = optional kwargs for func f(t,p,*args, **kwargs)
        ---------- OUTPUT VARIABLES -------
         JtWJ    = linearized Hessian matrix (inverse of covariance matrix)     (n x n)
         JtWdy   = linearized fitting vector                                    (n x m)
         Chi_sq = Chi-squared criteria: weighted sum of the squared residuals WSSR
         y_hat  = model evaluated with parameters 'p'                           (m x 1)
         J      = Jacobian of model, y_hat, with respect to parameters, p       (m x n)
        '''
        nonlocal func_calls, iteration      #So you can access variables inside function

        Npnt = len(y_dat)       #N of data points
        Npar = len(p)           #N of parameters

        y_hat = func(t, p, *args, **kwargs)
        func_calls += 1

        if iteration % (2*Npar) == 0 or dX2 > 0:       #every 2*M iteration use central/ or forward differences iterations ( or where
            J = lm_FD_J(func, t, p, y_hat, dp, args, kwargs)                              # X2 ( p +h ) > X2 ( p)
        else:
            J = lm_Broyden_J(p_old, y_old, J, p, y_hat) # rank - 1 update       #Le Broyden Jacobian in every n iteration


        delta_y = y_dat - y_hat # residual error between model and data
        Chi_sq = np.dot(delta_y, delta_y*weight)    #Chi squared as yT W y - scalar value

        JtWJ = J.T @ ( J * np.outer(weight, np.ones(Npar)))         # you get M X M matrix      J^T W J
        JtWdy = np.dot(J.T, weight*delta_y)                 # M x 1 array  J^T W ( y - y_hat ) (Right side of eq 12)

        return JtWJ, JtWdy, Chi_sq, y_hat, J


    def lm_FD_J(func, t, p, y, dp, args, kwargs):

        # J = lm_FD_J(func, t, p, y_hat, dp, args, kwargs)

        # partial derivatives (Jacobian) dy/dp for use with lm.m
        # computed via Finite Differences
        # Requires n or 2n function evaluations, n = number of nonzero values of dp
        # -------- INPUT VARIABLES ---------
        #   func = function of independent variables, 't', and parameters, 'p',
        #        returning the simulated model: y_hat = func(t,p,c)
        #   t  = independent variables (used as arg to func)                       (m x 1)
        #   p  = current parameter values                                          (n x 1)
        #   y  = func(t,p,c) initialised by user before each call to lm_FD_J       (m x 1)
        #  dp = fractional increment of p for numerical derivatives
        #       dp(j)>0 central differences calculated
        #      dp(j)<0 one sided differences calculated
        #      dp(j)=0 sets corresponding partials to zero; i.e. holds p(j) fixed
        #      Default:  0.001;
        # c  = optional vector of constants passed to y_hat = func(t,p,c)
        # ---------- OUTPUT VARIABLES -------
        # J  = Jacobian Matrix J(i,j)=dy(i)/dp(j)         i=1:n; j=1:m

        nonlocal func_calls

        n = len(y)
        m = len(p)


        J = np.zeros((n,m))
        delp = dp*( 1 + np.abs(p))         #\Delta_j ( 1 + |p_j|)          #Negative if dp < 0, positive otherwise

        p_min = np.copy(p)                  # Create copy of p, because python is weird sometimes...
        p_plus = np.copy(p)                 #one for central difference, one for p-delta p and p+delta p

        for j in range(m):

            p_plus[j] = p[j] + delp[j]  # p - delt p ( če je dp < 0 ) in p + delt p ( dp > 0 )

            if dp != 0:
                y_plus = func(t,p_plus,*args, **kwargs)         # calculate y(t; p +- \Delta p_j )
                func_calls += 1

                if dp < 0:  #backwards difference
                    J[:,j] = (y_plus - y)/delp[j]    # y(p- dp) - y(p)/ -dp --> y(p) - y(p-dp)/ dp  BACKWARDS

                else:       #central difference
                    p_min[j] = p[j] - delp[j]  # p - \Delta p
                    y_min = func(t,p_min,*args, **kwargs)   # calculate y(t; p - \Delta p_j )
                    func_calls += 1
                    J[:,j] = (y_plus - y_min)/ (2*delp[j])                 # y(p + dp) - y(p - dp)/ 2*dp   CENTRAL

            #Reset parameters to stock value
            p_plus[j] = p[j]
            p_min[j] = p[j]

        return J

    def lm_Broyden_J(p_old, y_old, J, p, y):
        # carry out a rank-1 update to the Jacobian matrix using Broyden's equation
        # ---------- INPUT VARIABLES -------
        # p_old = previous set of parameters                                     (n x 1)
        # y_old = model evaluation at previous set of parameters, y_hat(t;p_old) (m x 1)
        # J  = current version of the Jacobian matrix                            (m x n)
        # p     = current  set of parameters                                     (n x 1)
        # y     = model evaluation at current  set of parameters, y_hat(t;p)     (m x 1)
        # ---------- OUTPUT VARIABLES -------
        # J = rank-1 update to Jacobian Matrix J(i,j)=dy(i)/dp(j)  i=1:n; j=1:m  (m x n)

        h = p - p_old
        J += np.outer ( ( y - y_old - J @ h), h ) / np.inner(h,h)

        return J


    # --------------------------------------------------------------------------------------

    func_calls = 0
    iteration = 0

    #Dolžina arraya argumentov

    M = len(p)          # Number of parameters
    N = len(x)          # Number of points

    x = np.asarray(x, dtype = float)   #Change to numpy array
    p = np.asarray(p, dtype = float)
    y = np.asarray(y, dtype = float)

    DoF = N - M + 1     # Degrees of freedom
    p_old = np.zeros(M) # Previous set of parameters
    y_old = np.zeros(N) # Previous set of y_model
    X2  = 1e13          # Big initial values of chi 2
    X2_old = 1e13
    J = np.zeros((N, M))  # Jacobian


    if pmin is None:
        pmin = np.full(M, -np.inf)     #Določi, da ni meje v -p smeri

    if pmin is not None and isinstance(pmin, (list, tuple, np.ndarray)):
        pmin = np.asarray(pmin, dtype = float)
        if len(pmin) != M:
            raise ValueError("Dolžina p_min arraya ni enaka številu parametrov!")

    if pmax is None :
        pmax = np.full(M, np.inf)      #Določi da ni meje v +p smeri

    if pmax is not None and isinstance(pmax, (list, tuple, np.ndarray)):
        pmax = np.asarray(pmax, dtype = float)
        if len(pmax) != M:
            raise ValueError("Dolžina p_max arraya ni enaka številu parametrov!")

    if np.any(pmax <= pmin):
        raise ValueError("Vrednosti max ne morejo biti manjše od min vrednosti")

    if weight is None:
        weight = np.ones(N)
        #weight = np.ones(N)/np.dot(y, y )


    if weight is not None and isinstance(weight, (list, tuple, np.ndarray)):
        weight = np.abs(np.asarray(weight))     #Naredi, da so pozitivne vrednosti
        if len(weight) != N:
            raise ValueError("Dolžina uteži ni enaka dolžini neodvisnih spremenljivk!")

    if len(y) != N:
        raise ValueError("Velikost x ni enaka velikosti y!")

    # Naredi dp array dolzine velikosti spremenljivk in idx vektor

    dp_ar = dp*np.ones(M)
    idx = list(range(M))
    stop = 0   #Termination value

    #Začetna evaluacija modelske funkcije

    y_init = fun(x,p, *args, **kwargs)

    #Inicializacija Jakobiana z diferenčno metodo

    JtWJ, JtWdy, X2, y_hat, J = lm_matx(fun, x, p_old, y_old, 1, J, p, y, weight, dp, args, kwargs)

    if np.max(np.abs(JtWdy)) < epsG:
        print('Začetni parametri so zelo blizu optimalnim. Good guess!')
        print(f' *** epsilon_1 = {epsG:.2e}')
        stop = 1

    # Kako updejtat lambdo, ki je pomembna pri tem ali imaš Gradient descent ali GNM

    switcher = { 1: lam0,                           #Levenberg Marquadt update. ( chapter 4.1.1)
                 2: lam0 * np.diag(JtWJ).max(),     #Quadratic and Nielsen ( chapter 4.1.1)
                 3: lam0 * np.diag(JtWJ).max()  }

    #Choose lambda based on UpdateType, default 1
    lam = switcher.get(UpdateType)

    #Set startin value of nu in case of Nielsen update
    nu = 2

    #Set new value of Chi square
    X2_old = X2

    #Start convergence history.
    cvg_hst = np.ones((MaxIter + 1, M + 3))

    # --------------------------- MAIN LOOP ---------------------------------------

    while( not stop and iteration < MaxIter):

        iteration += 1           # Spremeni na 1

        if UpdateType == 1:
            h = np.linalg.solve(JtWJ + lam*np.diag(np.diag(JtWJ)) , JtWdy)          #Eq 13. solving for h

        else:
            h = np.linalg.solve( JtWJ + lam*np.eye(M), JtWdy )                      #Eq. 12 solving for h

        # ----------  Ali je [p + h] boljsi od [p] ? -------------

        # Update the [idx] elements - ne vem zakaj je ta idx potreben
        p_try = p + h[idx]

        # Constraini - ful elegantna rešitev!!
        p_try = np.minimum(np.maximum(pmin, p_try), pmax)

        # residual of the model function and data

        delta_y = y - fun(x,p_try, *args, **kwargs)
        func_calls += 1

        # Break if you've got floating point error
        if (~np.isfinite(delta_y)).any():
            stop = 1
            break

        # Chi-square calculation
        X2_try = np.dot(delta_y, delta_y *weight)

        # If you chose Quadratic
        if (UpdateType == 2):
            # One step of quadratic line update in the h direction for minimum X2
            alpha = np.inner(JtWdy, h) / ( (X2_try - X2)/2 + 2*np.inner(JtWdy, h))
            h *= alpha

            # Update the [idx] elements
            p_try = p + h[idx]

            # Constraini
            p_try = np.minimum(np.maximum(pmin, p_try), pmax)

            # residual of the model function and data
            delta_y = y - fun(x, p_try, *args, **kwargs)
            func_calls += 1

            # Break if you've got floating point error
            if (~np.isfinite(delta_y)).any():
                stop = 1
                break

            # Chi square
            X2_try = np.dot(delta_y, delta_y * weight)


        # ----------- Calculate Rho metrics ------------

        if UpdateType == 1:
            #If LM, then rho is calculated according to equation 16
            rho  = (X2 - X2_try) / ( np.inner(h, ( np.dot( lam*np.diag(np.diag(JtWJ)), h )  + JtWdy ) ) )

        else:
            #If quadratic then according to equation 15
            rho = (X2 - X2_try) / ( np.inner(h, ( lam*h + JtWdy ) ) )

        #If rho is good enough, accept values
        if (rho > epsL) and not stop:
            #Calculate the difference for lambda calculation if case 2, before you update chi 2
            dX2 = X2 - X2_old

            #Update chi2:
            X2_old = X2

            # Update p_old and y_old but not with direct equal sign ( Pointers!!!) - use copy instead, just for safety measure

            p_old = np.copy(p)
            y_old = np.copy(y_hat)
            p = np.copy(p_try)

            #Update lambda
            if UpdateType == 1:            #Levenberg
                lam = max(lam/lamD, 10e-7)

            elif UpdateType == 2:           #Quadratic
                lam = max(lam/(1+alpha), 10e-7)

            elif UpdateType == 3:           #Nielsen
                lam = lam*max(1/3, 1 - (2*rho -1)**3)
                nu = 2

            # Evaluate Jacobian once           #Ne vem če zares rabim to dvakrat klicat, ampak bom pustil not just for sake of it
            #JtWJ, JtWdy, X2, y_hat, J = lm_matx(fun, x, p_old, y_old, dX2, J, p, y, weight, dp, args, kwargs)

        # If rho is not good enough, update differently
        else:
            X2 = X2_old     #Do not accept p_try

            if UpdateType == 1:     #Levenberg
                lam = min( lam * lamU, 1.e7)

            elif UpdateType == 2:  #Quadratic
                lam +=  abs((X2_try - X2) )/ (2*alpha)

            elif UpdateType == 3:   #Nielsen
                lam *= nu
                nu *= 2

            # I dont know, man... tole mi je ful shady, zato ker dejansko ne oceni Jacobiana po rank 1, ker maš tam or v funkciji

            # Evaluate Jacobian once again
            #if iteration % (4*M) == 0:
            #   JtWJ, JtWdy, X2, y_hat, J = lm_matx(fun, x, p_old, y_old, -1, J, p, y, weight, dp, args, kwargs)

            dX2 = -1

        # Se mi zdi da bolj elegantna rešitev, da potem samo enkrat kličeš funkcijo kle dol
        # Calculate Jacobian
        JtWJ, JtWdy, X2, y_hat, J = lm_matx(fun, x, p_old, y_old, dX2, J, p, y, weight, dp, args, kwargs)

        if verbose is True:
            print(f'Iter : {iteration}, #N_func: {func_calls}, Reduced Chi square: {X2 / DoF:.3f}, | lambda = {lam:.2e}')
            print(f'Parameters : {p}')
            print(f'dp / p :  {abs(h)/abs(p)}')
            print("")

        # Update convergence History
        cvg_hst_ar = np.concatenate((p, np.asarray([func_calls, X2/DoF, lam]) ))       #Test pol k dela
        cvg_hst[iteration, :] = cvg_hst_ar

        # Breaking the chains of opression...

        #Convergence in gradient

        if (max(abs(JtWdy)) < epsG and  iteration > 2):
            print(' **** Convergence in r.h.s. ("JtWdy")  ****')
            print(f' \t \t \t epsG = {epsG}')
            stop = 1

        #Convergence in parameters
        if (max(np.abs(h)/(np.abs(p) + 1e-12)) < epsP and iteration > 2):
            print(' **** Convergence in Parameters ****')
            print(f'\t \t \t epsP = {epsP}')
            stop = 1

        #Convergence in reduced Chi-square
        if (X2 / DoF < epsChi and iteration > 2):
            print(' **** Convergence in reduced Chi-square  ****')
            print(f'\t \t \t epsChi= {epsChi}')
            stop = 1

        # Maximum iterations reached
        if (iteration == MaxIter):
            print(' !! Maximum Number of Iterations Reached Without Convergence !!')
            stop = 1

    #Recalculate weights, if there are none
    if np.var(weight) == 0:
        weight = ( DoF/ (np.dot(delta_y, delta_y)) ) * np.ones(N)

    #Calculate reduced chi^2
    redX2 = X2/DoF

    JtWJ, JtWdy, X2, y_hat, J = lm_matx(fun, x, p_old, y_old, -1, J, p, y, weight, dp, args, kwargs)

    #Calculate all the parameters for error evaluation
    covar_p = np.linalg.inv(JtWJ)
    sigma_p = np.sqrt( np.diagonal(covar_p))
    sigma_y = np.sqrt(np.diagonal(  np.dot(J , np.dot(covar_p, J.T)) ))
    corr_p = covar_p / np.inner(sigma_p, sigma_p)
    R_sq = np.corrcoef(y, y_hat)
    R_sq = R_sq[0, 1]**2

    # [ [p1, p2, p3...], func_calls, X2/DoF, lam  ] in one row
    cvg_hst = cvg_hst[:iteration, : ]

    return {'p': p, 'redX2': redX2, 'sigma_p': sigma_p, 'sigma_y': sigma_y, 'corr_p':corr_p, 'R_sq':R_sq, 'cvg_hst':cvg_hst}


